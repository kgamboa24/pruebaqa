package com.bdd.generico;

import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class WebDriverDOM extends PageObject {

    public WebDriver driver;

    public WebDriverDOM(WebDriver driver) {
        this.driver = driver;
    }

    public void click(By locator) {
        driver.findElement(locator).click();
    }

    public void sendKeys(String inputText, By locator) {
        driver.findElement(locator).sendKeys(inputText);
    }

    public List<WebElement> findElements(By locator, String selectText) {
        List<WebElement> elements = driver.findElements(locator);
        for (WebElement el : elements) {
            if (selectText.equals(el.getText())) {
                el.click();
            }
        }
        return elements;
    }

    public boolean isVisibleElement(By locator) {
        try {
            return driver.findElement(locator).isDisplayed();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getText(WebElement element) {
        return element.getText();
    }
     public void Close(){
         Set<String> handlesSet = driver.getWindowHandles();
         List<String> handlesList = new ArrayList<String>(handlesSet);
         driver.switchTo().window(handlesList.get(1));
         driver.close();
         driver.switchTo().window(handlesList.get(0));
     }
}
