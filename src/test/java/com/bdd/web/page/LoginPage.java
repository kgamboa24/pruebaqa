package com.bdd.web.page;

import com.bdd.generico.WebDriverDOM;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@DefaultUrl("https://www.amazon.com/-/es/")
public class LoginPage extends WebDriverDOM {

    By loginPageElement = By.xpath("//*[@id='nav-logo-sprites']");

    public LoginPage(WebDriver driver) {
        super(driver);
        driver.manage().window().maximize();
    }
    public boolean PaginaPrincipal() {
        boolean LoginPage = isVisibleElement(loginPageElement);
        return LoginPage;
    }
}