package com.bdd.web.page;

import com.bdd.generico.WebDriverDOM;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class amazonPage extends WebDriverDOM {
    By buscador = By.id("twotabsearchtextbox");
    By lupa = By.id("nav-search-submit-button");
  //  By marca = By.xpath("div[id='dropdown-content-s-all-filters'] div[class='a-section sf-refinements-section']");
    By marca = By.id("p_89/Disney");
    By quitarFiltro = By.xpath("//*[@data-action='sf-clear-filters'] //*[@class='a-link-normal aok-float-right s-no-underline sf-margin-top-mini']");

    public amazonPage(WebDriver driver) {
        super(driver);
    }

    public void buscarProducto(String producto) {
        sendKeys(producto, buscador);
    }

    public void clickBuscar() {
        waitFor(2).seconds();
        click(lupa);
    }


    public void filtrar(String product) {
        List<WebElement> lista_productos = findElements(marca, product);
        waitFor(2).seconds();
        click(marca);
        WebDriverWait wait = new WebDriverWait(driver, 120);

        for (WebElement producto : lista_productos) {
        }
    }

    public void quitarFiltro(){
        click(quitarFiltro);
    }
}


