package com.bdd.web.step;


import com.bdd.web.page.LoginPage;
import net.thucydides.core.annotations.Step;

public class LoginSteps {

    LoginPage loginPage;
    @Step
    public void que_cargo_la_pagina_de_amazon() {
        loginPage.setDefaultBaseUrl("https://www.amazon.com/-/es/");
        loginPage.open();
        loginPage.PaginaPrincipal();
    }

}