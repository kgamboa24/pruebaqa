package com.bdd.web.step;
import com.bdd.web.page.amazonPage;
import net.thucydides.core.annotations.Step;

public class amazonSteps {
   amazonPage amazonPage;

    @Step
    public void ingreso_la_busqueda_de_un_producto(String producto) {
    amazonPage.buscarProducto(producto);
    amazonPage.clickBuscar();
    }

    @Step
    public void filtrar_por_marca_solicitada(String marca) {
     amazonPage.filtrar(marca);
    }

    @Step
    public void validar_los_productos_por_nombre_y_precio() {

    }

    @Step
    public void quitar_el_filtro_agregado() {
     amazonPage.quitarFiltro();
    }

    @Step
    public void volver_validar_los_productos_por_nombre_y_precio() {

    }

}
