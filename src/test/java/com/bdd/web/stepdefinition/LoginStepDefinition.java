package com.bdd.web.stepdefinition;
import com.bdd.web.step.LoginSteps;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;

public class LoginStepDefinition extends ScenarioSteps {

    @Steps
    LoginSteps loginStepss;

    @Given("^que cargo la pagina de amazon$")
    public void que_cargo_la_pagina_de_amazon() {
        loginStepss.que_cargo_la_pagina_de_amazon();
    }
}