package com.bdd.web.stepdefinition;

import com.bdd.web.step.amazonSteps;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;

public class amazonStepDefinitions extends ScenarioSteps {

    @Steps
    amazonSteps amazonprodSteps;

    @When("^ingreso la busqueda de un producto \"([^\"]*)\"$")
    public void ingreso_la_busqueda_de_un_producto(String producto) {
    amazonprodSteps.ingreso_la_busqueda_de_un_producto(producto);
    }


    @When("^filtrar por marca solicitada \"([^\"]*)\"$")
    public void filtrar_por_marca_solicitada(String marca) {
        amazonprodSteps.filtrar_por_marca_solicitada(marca);
    }

    @When("^validar los productos por nombre y precio$")
    public void validar_los_productos_por_nombre_y_precio() {
        amazonprodSteps.validar_los_productos_por_nombre_y_precio();
    }

    @When("^quitar el filtro agregado$")
    public void quitar_el_filtro_agregado() {
        amazonprodSteps.quitar_el_filtro_agregado();
    }

    @Then("^volver a validar los productos por nombre y precio$")
    public void volver_validar_los_productos_por_nombre_y_precio() {
        amazonprodSteps.volver_validar_los_productos_por_nombre_y_precio();
    }
}
