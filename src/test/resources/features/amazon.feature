#language: en
@Cucumber
Feature:
  Como Un usuario comun
  Quiero acceder a la pagina de amazon
  para filtrar los productos

  @BuscarProducto
  Scenario Outline: seleccionar productos de amazon
    Given que cargo la pagina de amazon
    When ingreso la busqueda de un producto "<sProducto>"
    And filtrar por marca solicitada "<sMarca>"
    And validar los productos por nombre y precio
    And quitar el filtro agregado
    Then volver a validar los productos por nombre y precio
    Examples:
      | sProducto  | sMarca |
      | zapatillas | Disney |