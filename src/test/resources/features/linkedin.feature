@LOGIN_LINKEDIN
Feature: casos de prueba del inicio de sesion de linkedin

  @LOGIN
  Scenario Outline: Iniciar sesion con credenciales correcta
    When ingresar a la web de linkedin
    And ingresar usuario "<sUsuario>" y contraseña "<sPassword>"
    And seleccionar el boton iniciar sesion
    Then validar el dashboard de inicio
    Examples:
      | sUsuario         | sPassword |
      | prueba@gmail.com | admin123  |

  @LOGIN
  Scenario Outline: Iniciar sesion con credenciales incorrecta
    When ingresar a la web de linkedin
    And ingresar usuario "<sUsuario>" y contraseña "<sPassword>"
    And seleccionar el boton iniciar sesion
    Then validar mensaje de error "<sMensaje>"
    Examples:
      | sUsuario         | sPassword | sMensaje                                                                                  |
      | prueba@gmail.com | admin123  | "Esa no es la contraseña. Vuelve a intentarlo o inicia sesión con un enlace de uso único" |


  @LOGIN
  Scenario Outline: Iniciar sesion con la opcion de recuperar contraseña
    When ingresar a la web de linkedin
    And ingresar usuario "<sUsuario>" y contraseña "<sPassword>"
    And seleccionar la opcion olvido la contraseña
    And ingresar correo electronico para recuperar contraseña
    And seleccionar el boton recuperar contraseña
    Examples:
      | sUsuario         | sPassword |
      | prueba@gmail.com | admin123  |

  @LOGIN
  Scenario Outline: Iniciar sesion con el campo usuario vacio
    When ingresar a la web de linkedin
    And ingresar usuario "<sUsuario>" y contraseña "<sPassword>"
    And seleccionar el boton iniciar sesion
    Then validar mensaje de error "<sMensaje>"
    Examples:
      | sUsuario         | sPassword | sMensaje                                                  |
      | prueba@gmail.com | admin123  | "Introduce un correo electrónico o un número de teléfono" |

  @LOGIN
  Scenario Outline: Iniciar sesion con el campo contraseña vacio
    When ingresar a la web de linkedin
    And ingresar usuario "<sUsuario>" y contraseña "<sPassword>"
    And seleccionar el boton iniciar sesion
    Then validar mensaje de error "<sMensaje>"
    Examples:
      | sUsuario         | sPassword | sMensaje                    |
      | prueba@gmail.com | admin123  | "Introduce una contraseña." |

  @LOGIN
  Scenario Outline: crear nuevo usuario de linkedin
    When ingresar a la web de linkedin
    And seleccionar la opcion unirse ahora
    And ingresar correo "<sCorreo>" y contraseña "<sPassword>"
    And seleccionar el boton aceptar
    And ingresar nombres "<sNombres>" y apellidos "<sApellidos>"
    And seleccionar el boton continuar
    And realizar la verificacion de seguridad y click en aceptar
    Then validar que se registro el correo correctamente
    Examples:
      | sCorreo          | sPassword | sNombres | sApellidos |
      | prueba@gmail.com | admin123  | admin    | admin2     |

  @LOGIN
  Scenario : Iniciar sesion con la opcion apple
    When ingresar a la web de linkedin
    And seleccionar el boton iniciar sesion con apple
    And ingresar codigo de apple ID
    Then validar el inicio de sesion correcto
